
public class SoundAlarm implements AlarmListener {

	@Override
	public void alarmTurneOn(EnterPinEvent event) {
		System.out.println("AlarmOn");
	}

	@Override
	public void alarmTurnedOff(EnterPinEvent event) {
		System.out.println("AlarmOff");
	}

}
