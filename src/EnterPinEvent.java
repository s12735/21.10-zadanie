import java.text.SimpleDateFormat;
import java.util.Date;

public class EnterPinEvent {
	private Alarm alarm;
	private Date eventDate;
	 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public EnterPinEvent(Alarm alarm){
		this.alarm = alarm;
		this.eventDate = new Date();
		System.out.print(df.format(this.eventDate) + " ");
	}
	
	public Alarm getAlarm(){
		return this.alarm;
	}
	
	public Date getDate(){
		return this.eventDate;
	}
}
