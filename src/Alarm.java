import java.util.ArrayList;
import java.util.List;

public class Alarm {
	private String pin;
	private List<AlarmListener> listOfAlarm;
	
	public Alarm() {
		this.pin = "admin";
		this.listOfAlarm = new ArrayList<>();
		autoAddListener();
	}
	
	public Alarm(String pin){
		this.pin = pin;
		this.listOfAlarm = new ArrayList<>();
		autoAddListener();
	}

	public void addListener(AlarmListener listener) {
		this.listOfAlarm.add(listener);
	}

	public void removeListener(AlarmListener listener) {
		this.listOfAlarm.remove(listener);
	}

	public void enterPin(String pin) {
		if(this.pin.equals(pin)){
			this.correctEnteredPin();
		}
		else{
			this.wrongEnteredPin();
		}
	}
	
	private void autoAddListener(){
		this.listOfAlarm.add(new SoundAlarm());
		this.listOfAlarm.add(new Dog());
	}

	private void wrongEnteredPin() {
		for (AlarmListener a : listOfAlarm){
			a.alarmTurneOn(new EnterPinEvent(this));
		}
	}

	private void correctEnteredPin() {
		for (AlarmListener a : listOfAlarm){
			a.alarmTurnedOff(new EnterPinEvent(this));
		}
	}
}
