
public interface AlarmListener {
	public void alarmTurneOn(EnterPinEvent event);
	public void alarmTurnedOff(EnterPinEvent event);
}
